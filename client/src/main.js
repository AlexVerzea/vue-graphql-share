import "@babel/polyfill";
import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";

// Register FormAlert globally throughout the app.
import FormAlert from "./components/Shared/FormAlert.vue";
Vue.component("form-alert", FormAlert);

// Setup ApolloClient
import ApolloClient from "apollo-boost";
import VueApollo from "vue-apollo";
Vue.use(VueApollo);
export const defaultClient = new ApolloClient({
  uri: "https://vue-graphql-share-bbgkypfdcv.now.sh/graphql",

  // Include the auth token with requests that we made to Backend.
  fetchOptions: {
    credentials: "include"
  },
  request: async operation => {
    // If no token with key of 'token' in localStorage, add it.
    if (!localStorage.token) {
      localStorage.setItem("token", "");
    }

    // Operation adds the token to an authorization header, which is sent to Backend.
    operation.setContext({
      headers: {
        authorization: localStorage.getItem("token")
      }
    });
  },
  onError: ({ graphQLErrors, networkError }) => {
    if (networkError) {
      console.log("[NETWORK ERROR]", networkError);
    }
    if (graphQLErrors) {
      for (let err of graphQLErrors) {
        console.dir(err);

        // Set Auth Error in state (to show in snackbar).
        if (err.name === "AuthenticationError") {
          store.commit("setAuthError", err);
          // Signout User (to clear token).
          store.dispatch("signoutUser");
        }
      }
    }
  }
});
const apolloProvider = new VueApollo({ defaultClient });

Vue.config.productionTip = false;

new Vue({
  provide: apolloProvider.provide(),
  router,
  store,
  render: h => h(App),
  created() {
    // Execute getCurrentUser query.
    this.$store.dispatch("getCurrentUser");
  }
}).$mount("#app");
